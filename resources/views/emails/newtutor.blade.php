<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>New Module tutor</title>
</head>
<body>
    <p>Hello!</p>
    
    <p>You have been assigned as the tutor for the <b>{{ $module }}</b> for the class <b>{{ $class }}</b></p>
    
    <p>Thank you!</p>
</body>
</html>