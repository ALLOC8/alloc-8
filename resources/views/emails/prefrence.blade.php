<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Deadline for all to set their prefrence</title>
</head>
<body>
    <p>Hello!</p>
    
    <p>This message is to inform you that module prefrence for the {{ $sem }} Semester for the year {{ $year }} has been activated and you are asked to complete it by the given deadline</p>
    
    <p><strong>Deadling: {{ $date }} </strong></p>
    
    <p>Thank you!</p>
</body>
</html>