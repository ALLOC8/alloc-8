<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Removed as Module Coordinator</title>
</head>
<body>
    <p>Hello!</p>
    
    <p>You have been removed as the module coordinator for </p>
    
    <p><strong>Module:</strong> {{ $module }}</p>
    
    
    <p>Thank you!</p>
</body>
</html>