<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Deadline for all to set their prefrence</title>
</head>
<body>
    <p>Hello!</p>
    
    <p>This message is to inform you that module allocations for the {{ $sem }} Semester for the year {{ $year }} has been completed</p>
    <p>Please Log on to GCIT alloc8 to view your allocations</p>
    
    <p>Thank you!</p>
</body>
</html>