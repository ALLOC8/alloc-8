<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrjGroups extends Model
{
    use HasFactory;

    protected $fillable = [
        'prj_code',
        'semester',
        'year',
        'guide_id',
        'guide_name',
        'class_id',
        'class_code',
    ];
}
