<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;
    protected $fillable = [
        'course_name', // Add any other fields that are fillable
        'semesters',
        'course_leader_name',
        'course_leader_id',
        // Add other fields here as needed
    ];
}
