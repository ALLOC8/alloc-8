<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeachingRecord extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', // Add any other fields that are fillable
        'userName',
        'semester',
        'module_code',
        'classCode',
        'class_id',
        'year',
        'classHours'

        // Add other fields here as needed
    ];
}
