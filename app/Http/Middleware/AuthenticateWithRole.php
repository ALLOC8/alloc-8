<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AuthenticateWithRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response)  $next
     */
    public function handle(Request $request, Closure $next, string $role): mixed
    {
        error_log("getting into middleware ".$request->user()->role);
        error_log("role ".$role);

        if (!$request->user()) {
            return redirect()->route('login')->with('error', 'Please login!');
        }else if ($request->user()->role !== $role) {
            if ($request->user()->role === 'admin'){
                return redirect('/home')->with('error','Permission Denied!');
            }else if($request->user()->role === 'user'){
                return redirect('/dashboard')->with('error','Permission Denied!');
            }
        }

        return $next($request);
    }
}

