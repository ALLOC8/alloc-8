<?php

namespace App\Http\Controllers;

use App\Models\User;
use Barryvdh\LaravelIdeHelper\UsesResolver;
use Hash;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Mail;
use Str;

class UserController extends Controller
{
    public function AddUsers(Request $request)
    {
        $data = $request->validate([
                'name' => 'required',
                'email' => 'required|email|unique:App\Models\User,email',
        ]);
        $randomPassword = Str::random(8);

        // Add the generated password to the data array
        $data['password'] = Hash::make($randomPassword); // Hash the password

        $data['role'] = 'user';

        $user = User::factory()->create($data);

        $this->sendPasswordEmail($data['email'], $randomPassword);

        return response()->json(['message' => 'User added successfully', 'user' => $user], 201);
    }
    private function sendPasswordEmail($email, $password)
    {
        // You can customize the email view and subject as needed
        Mail::send('emails.newUserPassword', ['password' => $password], function ($message) use ($email) {
            $message->to($email)->subject('Your New User Password');
        });
    }
    public function deleteUsers($id)
        {
            $item = User::find($id);

            if (!$item) {
                return response()->json(['message' => 'User not found'], 404);
            }

            $item->delete();

            return response()->json(['message' => 'User deleted'], 200);
        }

        public function editUser(Request $request, User $user)
        {

            $this->validate($request, [
                'name' => 'required|string',
            ]);

            // Update the user's information
            $user->update([
                'name' => $request->input('name'),
            ]);

            return response()->json(['message' => 'User updated successfully']);
        }
        public function changePassword(Request $request)
        {
            $data = $request->validate([
                'oldPassword' => 'required',
                'newPassword' => 'required|min:8', 
                'confirmPassword' => 'required|same:newPassword|min:8',
            ]);
            $user = auth()->user();
            $data['password'] = Hash::make($request->input('newPassword')); // Hash the password

            if (!Hash::check($data['oldPassword'], $user->password)) {
                return Inertia::render('ChangePassword', [
                    'errors' => ['oldPassword1' => 'The old password is incorrect.']
                ])->toResponse($request);
            } else {
                User::find($user->id)->update([
                    'password' => $data['password']
                ]);
            }
        }   
}
