<?php

namespace App\Http\Controllers;

use App\Models\state;
use App\Models\User;
use App\Models\WeeklyHours;
use Illuminate\Http\Request;
use Mail;
use Validator;

class statusController extends Controller
{
    public function statusUpdate(Request $request)
    {
        // Validate incoming data
        $request->validate([
            'date' => 'required',
            'year' => 'required',
            'semester' => 'required',
        ]);

        // Check if 'modules' key is present and is an array
        $modules = $request->input('classModule', []);
        $date = $request->input('date');
        $year = $request->input('year');
        $sem = $request->input('semester');

        // Convert semRearranged to a JSON string
        $moduleJson = json_encode($modules);

        $course = state::create([
            'date' => $date,
            'modules' => $moduleJson, // Insert the JSON string
            'year' => $year,
            'semester' => $sem,
            'state' => true,
        ]);

        $users = User::where('role', 'user')->get();

        // Check if there are users before sending emails
        if ($users->isEmpty()) {
            return response()->json(['message' => 'No users found with role "user"'], 200);
        }

        foreach ($users as $user) {
            $username = $user->name;
            $user_id = $user->id;

            WeeklyHours::create([
                'user_id' =>$user_id,
                'user_name' =>$username,		
                'year'	=> $year,
                'semester'	=> $sem,
                'class_hours' => 0,	
                'prj_groups' => 0,

            ]);
        }

        // Extract email addresses
        $emailAddresses = $users->pluck('email')->toArray();

        // Send email to users with role 'user' using bcc
        Mail::send('emails.prefrence', ['date' => $date, 'year' => $year, 'sem' => $sem], function ($message) use ($emailAddresses) {
            $message->bcc($emailAddresses)->subject('Prefrence setting');
        });

        return response()->json($course, 201);
    }
    public function editState(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'moduleCode' => 'required',
            'class' => 'required',
            'sem' => 'required',
            'year' => 'required',
        ]);
        
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        // Use where() with an array to specify multiple conditions
        $module = State::where(['semester' => $request->input('sem'), 'year' => $request->input('year')])->first();

        if (!$module) {
            return response()->json(['error' => 'Module not found'], 404);
        }

        // Decode the JSON string to an associative array
        $moduleData = json_decode($module->modules, true);

        // Update the module data based on input
        $moduleData[$request->input('class')][$request->input('moduleCode')] = $request->input('user_id');

        // Encode the array back to JSON
        $moduleJson = json_encode($moduleData);

        // Update the 'modules' column in the database
        $module->update(['modules' => $moduleJson]);

        return response()->json($module, 200);
    }
}
