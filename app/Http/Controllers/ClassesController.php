<?php

namespace App\Http\Controllers;

use App\Models\Classes;
use Illuminate\Http\Request;
use Psy\Readline\Hoa\Console;

class ClassesController extends Controller
{
    public function addClass(Request $request)
    {
        // Validate the request data
        $request->validate([
            'c_name' => 'required',
            'c_id' => 'required', // Assuming 'c_id' should be unique in the 'classes' table
            's_year' => 'required',
            'e_year' => 'required',
            'strength' => 'required',
        ]);
    
        // Create a new class
        Classes::create([
            'course_name' => $request->input('c_name'),
            'course_id' => $request->input('c_id'),
            'start_year' => $request->input('s_year'),
            'end_year' => $request->input('e_year'),
            'strength' => $request->input('strength'),
        ]);
    
    }
    public function deleteClass($id)
        {
            $item = Classes::find($id);

            if (!$item) {
                return response()->json(['message' => 'class not found'], 404);
            }

            $item->delete();
        }
        
        public function editClassStrength(Request $request)
        {
            $prjGroup = Classes::where([
                'id' => $request->input('class_id'),
            ])->update(
                [
                    'strength' => $request->input('strength'),
                ]
            );
        }    
}
