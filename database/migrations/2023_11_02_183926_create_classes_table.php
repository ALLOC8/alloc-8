<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
{
    Schema::create('classes', function (Blueprint $table) {
        $table->id();
        $table->year('start_year');
        $table->year('end_year');
        $table->unsignedBigInteger('course_id');
        $table->string('course_name');
        $table->string('section')->nullable(); // Allow NULL for the 'section' field
        $table->string('strength');
        $table->timestamps();

        $table->unique(['start_year', 'end_year', 'course_name', 'section']);

        $table->foreign('course_id')->references('id')->on('courses');
    });

    DB::unprepared('
        CREATE TRIGGER before_insert_classes
        BEFORE INSERT ON classes FOR EACH ROW
        BEGIN
            DECLARE next_section INT;

            -- Calculate the next numeric section
            SELECT IFNULL(MAX(CAST(SUBSTRING(section, 1) AS SIGNED)), 0) + 1 INTO next_section
            FROM classes
            WHERE course_name = NEW.course_name
            AND start_year = NEW.start_year
            AND end_year = NEW.end_year;

            -- If there is no existing section, set it to 1
            IF next_section = 0 THEN
                SET next_section = 1;
            END IF;

            -- Convert the numeric section to the subsequent alphabet
            SET NEW.section = next_section;
        END
    ');

}

public function down()
{
    Schema::dropIfExists('classes');
}

};